public class MyFirstClass {
    public static void main(String[] args) {
        String model = "Ford";
        String make = "F150";
        double engineSize = 3.6;
        byte gear = 1;

        short speed =  (short)(gear*20);
        System.out.println("Model: " + model);
        System.out.println("Make: " + make);
        System.out.println("Engine size: " + engineSize);
        System.out.println("Speed: " + speed);
        if (engineSize <= 1.3) {
            System.out.println("Car is weak");
        }
        else {
            System.out.println("Car is powerful");
            }
        int count = 0;
        for(int i = 1900; i<2001; i++){
            if(i % 4 == 0){
                System.out.println(i);
                count++;
                if(count == 5){
                    System.out.println("Finished");
                    break;
                }
            }
        }
    }
}
