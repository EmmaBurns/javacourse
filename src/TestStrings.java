import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TestStrings {
    public static void main(String[] args) {
        StringBuilder fileName = new StringBuilder("example.doc");
        fileName.replace(fileName.length()-3, fileName.length(), "bak");
        System.out.println(fileName);

        String test1 = "Korma";
        String test2 = "Maya";

        if (test1.equals(test2))
            System.out.println("content the same");

        String further = (test1.compareTo(test2) > 0) ? test1 : test2;

        System.out.println("string further in dictionary is " + further);

        String test = "the quick brown fox swallowed down the lazy chicken";

        int count = 0;
        for (int i=0; i<test.length()-2; i++)
        {
            if (test.substring(i, i+2).equals("ow"))
                count++;
        }
        System.out.printf("ow appears %d times\n", count);


        StringBuilder palindrome = new StringBuilder("racecar");
        StringBuilder palindrome2 = new StringBuilder(palindrome);
        if (palindrome.reverse().toString().equalsIgnoreCase(palindrome2.toString()))
            System.out.println("palindrome");


        Format yearOnly = new SimpleDateFormat("yyyy");
        Format dayMonthYear = new SimpleDateFormat("dd MM yyyy");
        Format time = new SimpleDateFormat("hh mm");
        System.out.println(yearOnly.format(new Date()));
        System.out.println(dayMonthYear.format(new Date()));
        System.out.println(time.format(new Date()));
    }
}
