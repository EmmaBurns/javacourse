public class TestAccount2 {
    public static void main(String[] args) {
        Account[] arrayOfAccounts = new Account[5];

        double[] amounts = {23,54,72,345,34};
        String[] names = {"Picard", "Ryker", "Worf", "Troy", "Data"};

        Account.setInterestRate(1.6);
        for (int i = 0; i < arrayOfAccounts.length; i++){
            arrayOfAccounts[i] = new Account(amounts[i], names[i]);
            System.out.println("Name: " + arrayOfAccounts[i].getName() + "| Balance: " + arrayOfAccounts[i].getBalance());
            arrayOfAccounts[i].addInterest();
            System.out.println("Name: " + arrayOfAccounts[i].getName() + "| Balance: " + arrayOfAccounts[i].getBalance());

        }

        System.out.println(arrayOfAccounts[2].withdraw());
        System.out.println("Name: " + arrayOfAccounts[2].getName() + "| Balance: " + arrayOfAccounts[2].getBalance());
        System.out.println(arrayOfAccounts[2].withdraw(1000));
        System.out.println("Name: " + arrayOfAccounts[2].getName() + "| Balance: " + arrayOfAccounts[2].getBalance());

    }
}
