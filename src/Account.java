public class Account {
    private double balance;
    private String name;
    static double interestRate=1.5;


    public Account() {
        this.balance = 50;
        this.name = "Emma";
    }

    public Account(double balance, String name) {
        this.balance = balance;
        this.name = name;
    }

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void addInterest(){
        this.balance *= interestRate;
    }

    public boolean withdraw(double amount){
        if (amount <= this.balance){
            this.balance -= amount;
            return true;
        }
        return false;
    }
    public boolean withdraw(){
        return withdraw(20);
    }
}
